add the following to the config:

### yaml ###

vich_uploader:
    db_driver: orm
    
    mappings:
        upload_file:
            uri_prefix: /files
            upload_destination: %kernel.root_dir%/../web/files
            inject_on_load: false
            delete_on_update: true
            delete_on_remove: true
            
        user_uploads:
            uri_prefix: /uploads
            upload_destination: %kernel.root_dir%/../web/uploads
            inject_on_load: false
            delete_on_update: true
            delete_on_remove: true